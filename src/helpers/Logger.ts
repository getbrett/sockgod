import * as winston from 'winston';

export default class Logger {

    public static debug(msg: string) {
        Logger.logger.debug(msg);
    }

    public static info(msg: string) {
        Logger.logger.info(msg);
    }

    public static warn(msg: string) {
        Logger.logger.warn(msg);
    }

    public static error(msg: string, e: Error) {
        Logger.logger.error(msg, e);
    }

    private static colorizer = winston.format.colorize();
    private static logger = winston.createLogger({
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.simple(),
            winston.format.printf((msg) =>
                `${msg.timestamp} - ${Logger.colorizer.colorize(msg.level, `[${msg.level}]:`)} ${msg.message}`,
            ),
        ),
        level: 'info',
        transports: [
            new winston.transports.Console({}),
        ],
    });
}
