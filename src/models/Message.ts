export interface IMessage {
    name: string;
    message: string;
}

export class UserMessage implements IMessage {
    public name: string;
    public message: string;

    constructor(payload: string) {
        const data = JSON.parse(payload);

        if (!data.name || !data.message) {
            throw new Error(`Invalid Message payload received: ${payload}`);
        }

        this.name = data.name;
        this.message = data.message;
    }
}

export default UserMessage;
