import { expect } from 'chai';
import 'mocha';

import UserMessage from './Message';

describe('Message', () => {
    it('should accept a name and message', () => {
        const result = new UserMessage(JSON.stringify({
            message: 'Hello world!',
            name: 'Brett',
        }));

        expect(result.name).to.equal('Brett');
        expect(result.message).to.equal('Hello world!');
    });
});
