import * as WebSocket from 'ws';
import Logger from './helpers/Logger';
import { UserMessage } from './models/Message';

// Create a server
const port: number = parseInt(process.env.port, 10) || 3000;
const server: WebSocket.Server = new WebSocket.Server({ port });

// Add a handler when client connects to the server
server.on('connection', (ws) => {
    Logger.info('New connection');

    ws.on('message', (message) => {
        try {
            const userMessage: UserMessage = new UserMessage(message.toString());
            broadcast(JSON.stringify(userMessage));
        } catch (e) {
            Logger.error(e.message, e);
        }
    });
});

function broadcast(data: string): void {
    server.clients.forEach((client) => {
        client.send(data);
    });
}

Logger.info(`Server is running on port: ${port}`);
